package pl.documentfinder.algorithm;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import pl.documentfinder.model.Document;
import pl.documentfinder.service.DocumentService;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class TFIDFSolTest {

  private DocumentService documentService = new DocumentService();

  @Test
  public void should_have_3_elements_in_db() {
    List<Document> someDocuments = getSomeDocuments();

    TFIDFSol sol = new TFIDFSol(someDocuments, Collections.emptyList());
    List<Document> db = sol.getDocumentCollection();
    assertThat(db).hasSize(someDocuments.size());

    for (int i = 0; i < db.size(); i++) {
      assertThat(db.get(i).getValue()).isEqualTo(someDocuments.get(i).getValue());
    }
  }

  @Test
  public void should_calculate_tf() {
    TFIDFSol sol = new TFIDFSol(getSomeDocumentsFromEziClass(), getSomeTermsFromEziClass());
    ImmutableMap<String, Double> expectedTf =
        ImmutableMap.<String, Double>builder()
            .put("information", 0.5)
            .put("retrieval", 0.5)
            .put("agency", 1.0)
            .build();

    Map<String, Double> tf =
        sol.calculateTF(Document.fromString("agency information retrieval agency"));

    for (String key : tf.keySet()) {
      assertThat(tf.get(key)).isEqualTo(expectedTf.get(key));
    }

    assertThat(tf.size()).isEqualTo(getSomeTermsFromEziClass().size());
  }

  @Test
  public void should_calculate_more_complex_tf() {

    ImmutableMap<String, Double> expectedTf =
        ImmutableMap.<String, Double>builder()
            .put("bee", 0.)
            .put("wasp", 0.)
            .put("fli", 1.0)
            .put("fruit", 0.5)
            .put("like", 1.)
            .build();

    List<Document> stemmedDocuments = documentService.stemDocuments(getSomeDocumentsFromExcel());
    List<Document> stemmedTerms = documentService.stemDocuments(getSomeTermsFromExcel());

    TFIDFSol sol = new TFIDFSol(stemmedDocuments, stemmedTerms);
    String stemmedDocument = stemmedDocuments.get(0).getValue();

    Map<String, Double> tf = sol.calculateTF(Document.fromString(stemmedDocument));

    assertThat(tf.size()).isEqualTo(getSomeTermsFromExcel().size());

    for (String key : expectedTf.keySet()) {
      assertThat(tf.getOrDefault(key, 0.)).isEqualTo(expectedTf.get(key));
    }
  }

  @Test
  public void should_calculate_idf() {
    ImmutableMap<String, Double> expectedTf =
        ImmutableMap.<String, Double>builder()
            .put("bee", 0.3979400086720376)
            .put("wasp", 0.3979400086720376)
            .put("fli", 0.3979400086720376)
            .put("fruit", 0.5228787452803376)
            .put("like", 0.3979400086720376)
            .build();

    List<Document> stemmedDocuments = documentService.stemDocuments(getSomeDocumentsFromExcel());
    List<Document> stemmedTerms = documentService.stemDocuments(getSomeTermsFromExcel());

    TFIDFSol sol = new TFIDFSol(stemmedDocuments, stemmedTerms);
    Map<String, Double> idfMap = sol.getIdfMap();
    assertThat(idfMap.size()).isEqualTo(getSomeTermsFromExcel().size());

    for (String key : idfMap.keySet()) {
      assertThat(idfMap.get(key)).isEqualTo(expectedTf.get(key));
    }
  }

  @Test
  public void should_calculate_similarity() {
    Document queryDocument = Document.fromString("fruit flies");
    List<Document> stemmedDocuments = documentService.stemDocuments(getSomeDocumentsFromExcel());
    List<Document> stemmedTerms = documentService.stemDocuments(getSomeTermsFromExcel());

    TFIDFSol sol = new TFIDFSol(stemmedDocuments, stemmedTerms);

    Vector<DocScore> rank = sol.rank(queryDocument);
  }

  @Test
  public void should_give_feedback() {
    Document originalQuery = Document.fromString("run cat");

    List<Document> allDocuments = documentService.stemDocuments(getSomeRocchioDocuments());
    List<Document> stemmedTerms = documentService.stemDocuments(getSomeRocchioTerms());

    TFIDFSol sol = new TFIDFSol(allDocuments, stemmedTerms);
    sol.getRelevanceFeedback(
        originalQuery, getSomeRelevantDocuments(), getSomeNonRelevantDocuments());

//    fail("not implemented");
  }

  private List<Document> getSomeDocumentsFromEziClass() {
    return Stream.of(
            "information retrieval information retrieval",
            "retrieval retrieval retrieval retrieval",
            "agency information retrieval agency",
            "retrieval agency retrieval agency")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private List<Document> getSomeTermsFromEziClass() {
    return Stream.of("information", "retrieval", "agency")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private List<Document> getSomeDocumentsFromExcel() {
    return Stream.of(
            "Time flies like an arrow but fruit flies like a banana",
            "It's strange that bees and wasps don't like each other",
            "The flight attendant sprayed the cabin with a strange fruity aerosol",
            "Try not to carry a light, as wasps and bees may fly toward it",
            "Fruit flies fly around in swarms. When flying they flap their wings 220 times a second")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private List<Document> getSomeTermsFromExcel() {
    return Stream.of("bee", "wasp", "fli", "fruit", "like")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private List<Document> getSomeDocuments() {
    return Stream.of(
            "Equations",
            "Equations",
            "Algorithms Theory Implementation Application",
            "Partial Differential Equations",
            "Algorithms Introduction",
            "Introduction Systems Problems",
            "Problems Algorithms Implementation",
            "Methods Systems Ordinary Differential Equations",
            "Nonlinear Systems",
            "Ordinary Differential Equations",
            "Oscillation Theory Delay Differential Equations",
            "Oscillation Theory Delay Differential Equations",
            "Nonlinear Partial Differential Equations",
            "Methods Differential Equations",
            "Differential Equations",
            "Integral Problems",
            "Integral Application Theory")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private List<Document> getSomeRocchioTerms() {
    return Stream.of("run", "lion", "cat", "dog", "program")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private List<Document> getSomeRocchioDocuments() {
    return Stream.of(
            "run", "run", "run", "run", "lion", "lion", "lion", "lion", "cat", "cat", "program",
            "program", "program")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private Vector<Document> getSomeRelevantDocuments() {
    return Stream.of("run", "run", "lion", "lion", "cat")
        .map(Document::fromString)
        .collect(Collectors.toCollection(Vector::new));
  }

  private Vector<Document> getSomeNonRelevantDocuments() {
    return Stream.of("run", "run", "cat", "program", "program", "program")
        .map(Document::fromString)
        .collect(Collectors.toCollection(Vector::new));
  }
}
