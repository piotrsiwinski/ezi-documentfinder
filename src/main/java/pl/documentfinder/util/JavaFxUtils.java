package pl.documentfinder.util;

import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;

import java.util.List;

public class JavaFxUtils {
  private static final String NEW_LINE = "\n";
  private JavaFxUtils() {
    throw new AssertionError();
  }

  public static void showErrorAlert() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Cannot open file");
    alert.setHeaderText("Information Alert");
    String s = "Cannot open file, wrong Path";
    alert.setContentText(s);
    alert.show();
  }

  public static void showErrorAlert(String title, String header, String content) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);
    alert.show();
  }

  public static void setTextAreaText(TextArea area, List<?> list) {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < list.size(); i++) {
      Object e = list.get(i);
      builder.append("No. ")
          .append(i)
          .append(" ")
          .append(e.toString())
          .append(NEW_LINE);
    }
    area.setText(builder.toString());
  }
}
