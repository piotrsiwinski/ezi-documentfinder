package pl.documentfinder.util;

import java.util.regex.Pattern;

public class StringUtils {

  private static final Pattern pattern = Pattern.compile("\\p{Punct}");

  private StringUtils() {
    throw new AssertionError();
  }

  public static String toLowerCaseWithoutWhiteSpace(String s) {
    return s.toLowerCase().replaceAll(pattern.pattern(), "");
  }
}
