package pl.documentfinder.util;

import java.net.URL;

public class ResourceUtil {
  public static URL getResource(String path) {
    return ResourceUtil.class.getClassLoader().getResource(path);
  }
}
