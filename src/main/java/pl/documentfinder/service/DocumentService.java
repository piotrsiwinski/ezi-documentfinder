package pl.documentfinder.service;

import opennlp.tools.stemmer.PorterStemmer;
import opennlp.tools.stemmer.Stemmer;
import pl.documentfinder.model.Document;
import pl.documentfinder.model.Term;
import pl.documentfinder.util.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang.StringUtils.isBlank;
import static pl.documentfinder.util.FileUtil.readAllLines;
import static pl.documentfinder.util.StringUtils.toLowerCaseWithoutWhiteSpace;

public class DocumentService {
  private Stemmer stemmer = new PorterStemmer();

  public List<Document> getDocuments(File file) {
    return mapToDocumentList(readAllLines(file));
  }

  List<Document> mapToDocumentList(List<String> strings) {
    StringBuilder stringBuilder = new StringBuilder();
    List<Document> resultList = new LinkedList<>();
    for (String string : strings) {
      if (isBlank(string)) {
        resultList.add(Document.fromString(stringBuilder.toString()));
        stringBuilder.setLength(0);
        continue;
      }
      stringBuilder.append(string);
    }
    resultList.add(Document.fromString(stringBuilder.toString()));
    return resultList;
  }

  public List<Term> getTerms(File file) {
    return FileUtil.readAllLines(file)
        .stream()
        .map(s -> Term.builder().value(s).build())
        .collect(Collectors.toList());
  }

  public List<Document> stemDocuments(List<Document> documentsToStem) {
    List<Document> stemmedDocuments = new ArrayList<>(documentsToStem.size());
    for (Document document : documentsToStem) {
      stemmedDocuments.add(stemDocument(document));
    }
    return stemmedDocuments;
  }

  public Document stemDocument(Document document) {
    String text = toLowerCaseWithoutWhiteSpace(document.getValue());
    String[] line = text.split(" ");
    StringBuilder builder = new StringBuilder();
    for (String s : line) {
      builder.append(stemmer.stem(s)).append(" ");
    }
    return Document.fromString(builder.toString());
  }


}
