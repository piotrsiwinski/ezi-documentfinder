package pl.documentfinder.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import opennlp.tools.stemmer.PorterStemmer;
import pl.documentfinder.algorithm.DocScore;
import pl.documentfinder.algorithm.TFIDFSol;
import pl.documentfinder.model.Document;
import pl.documentfinder.model.Term;
import pl.documentfinder.service.DocumentService;
import pl.documentfinder.util.JavaFxUtils;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

import static pl.documentfinder.util.JavaFxUtils.setTextAreaText;

public class MainController {

  @FXML private Label resultLabel;
  @FXML private Button searchButton;
  @FXML private TextArea termTextArea;
  @FXML private TextArea rocchioResultTextArea;
  @FXML private TextField relevantDocsIdsTextField;
  @FXML private TextArea documentTextArea;
  @FXML private TextArea stemmedDocumentTextArea;
  @FXML private TextField queryTextField;
  @FXML private TextArea queryTextArea;
  @FXML private ScrollPane mainGridPane;

  private DocumentService fileService = new DocumentService();

  private List<Document> documents;
  private List<Document> stemmedDocuments;
  private List<Document> stemmedTerms;

  public void openDocumentSet(MouseEvent mouseEvent) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");

    try {
      // get file
      File file =
          Optional.ofNullable(fileChooser.showOpenDialog(mainGridPane.getScene().getWindow()))
              .orElseThrow(RuntimeException::new);
      // parse file to list of documents
      List<Document> rawDocuments = fileService.getDocuments(file);
      setTextAreaText(documentTextArea, rawDocuments);
      // get stemmed documents
      List<Document> stemDocuments = fileService.stemDocuments(rawDocuments);
      setTextAreaText(stemmedDocumentTextArea, stemDocuments);

      // keep documents in memory for further operations
      this.stemmedDocuments = stemDocuments;
      this.documents = rawDocuments;
    } catch (RuntimeException e) {
      JavaFxUtils.showErrorAlert();
    }
  }

  public void openTermSet(MouseEvent mouseEvent) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");
    try {
      File file =
          Optional.ofNullable(fileChooser.showOpenDialog(mainGridPane.getScene().getWindow()))
              .orElseThrow(RuntimeException::new);
      List<Term> terms = fileService.getTerms(file);

      PorterStemmer stemmer = new PorterStemmer();
      List<Term> stemmedWords = new LinkedList<>();
      for (Term t : terms) {
        String stem = stemmer.stem(t.getValue());
        stemmedWords.add(Term.builder().value(stem).build());
      }

      StringBuilder sb = new StringBuilder();
      stemmedWords.forEach(t -> sb.append(t).append("\n"));
      this.stemmedTerms =
          stemmedWords
              .stream()
              .map(e -> Document.fromString(e.getValue()))
              .collect(Collectors.toList());
      termTextArea.setText(sb.toString());
    } catch (RuntimeException ex) {
      JavaFxUtils.showErrorAlert();
    }
  }

  public void onSearchClicked(MouseEvent mouseEvent) {
    TFIDFSol tfidfSol = new TFIDFSol(this.stemmedDocuments, this.stemmedTerms);
    String text = this.queryTextField.getText();

    Document stemmedDocument = fileService.stemDocument(Document.fromString(text));
    Vector<DocScore> rank = tfidfSol.rank(stemmedDocument);

    StringBuilder stringBuilder = new StringBuilder();
    for (DocScore docScore : rank) {
      stringBuilder.append(
          String.format(
              "%d, %.5f, %s\n",
              docScore.getDocId(), docScore.getScore(), this.documents.get(docScore.getDocId())));
    }
    this.queryTextArea.setText(stringBuilder.toString());
    this.resultLabel.setText(String.format("Found: %d results", rank.size()));
  }

  public void onrelevantFeedbackClick(MouseEvent mouseEvent) {
    try{
      String text = relevantDocsIdsTextField.getText();
      List<Document> relevantDocumentList =
          Arrays.stream(text.replace(", ", " ").split(" "))
              .map(Integer::valueOf)
              .map(id -> documents.get(id))
              .collect(Collectors.toList());


      TFIDFSol tfidfSol = new TFIDFSol(this.stemmedDocuments, this.stemmedTerms);
      Vector<DocScore> rank = tfidfSol.getRelevanceFeedback(Document.fromString(queryTextField.getText()), new Vector<>(relevantDocumentList), null);

      StringBuilder stringBuilder = new StringBuilder();
      for (DocScore docScore : rank) {
        stringBuilder.append(
            String.format(
                "%d, %.5f, %s\n",
                docScore.getDocId(), docScore.getScore(), this.documents.get(docScore.getDocId())));
      }
      this.rocchioResultTextArea.setText(stringBuilder.toString());

    } catch (Exception e){
      JavaFxUtils.showErrorAlert("Wrong delimeter", "Wrong delimeter", "Use: docId, docId, i.e. 1, 2, 3, 4, 5");
    }

  }
}
