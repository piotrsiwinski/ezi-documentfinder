package pl.documentfinder.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Term {
  private String value;

  public static Term fromString(String s){
    return new Term(s.trim());
  }
}
