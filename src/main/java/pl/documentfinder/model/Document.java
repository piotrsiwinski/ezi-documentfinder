package pl.documentfinder.model;

import lombok.Value;

@Value
public class Document implements Comparable<Document>{
  private String value;

  public static Document fromString(String s) {
    return new Document(s.trim());
  }

  @Override
  public String toString() {
    return value;
  }

  @Override
  public int compareTo(Document o) {
    return this.value.compareTo(o.value);
  }
}
