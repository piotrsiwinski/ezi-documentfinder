package pl.documentfinder.algorithm;

import pl.documentfinder.model.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

public class TFIDFSol {

  private List<Document> documentCollection;
  private List<Document> termCollection;
  private TreeMap<String, Double> idfMap = new TreeMap<>();
  private Vector<Map<String, Double>> tfMap = new Vector<>();
  private Vector<Map<String, Double>> tfIdfMap = new Vector<>();

  public TFIDFSol(List<Document> documents, List<Document> terms) {
    documentCollection = requireNonNull(documents);
    termCollection = requireNonNull(terms);
//    documentCollection.sort(Document::compareTo);
//    termCollection.sort(Document::compareTo);
    init();
  }

  private void init() {
    documentCollection.forEach(d -> tfMap.add(calculateTF(d)));

    for (Document d : termCollection) {
      Double sum = 0.;
      for (Map<String, Double> map : tfMap) {
        sum += map.get(d.getValue());
      }
      idfMap.put(d.getValue(), calculateIdf(sum));
    }
    for (Map<String, Double> map : tfMap) {
      Map<String, Double> tfidfMap = new TreeMap<>();
      for (Map.Entry<String, Double> entry : map.entrySet()) {
        tfidfMap.put(entry.getKey(), calculateTfIdf(entry.getValue(), idfMap.get(entry.getKey())));
      }
      tfIdfMap.add(tfidfMap);
    }
  }

  double calculateIdf(double termFrequency) {
    if (termFrequency == 0.) {
      return 0;
    }
    return Math.log10(this.documentCollection.size() / termFrequency);
  }

  double calculateTfIdf(double tf, double idf) {
    return tf * idf;
  }

  Map<String, Double> getIdfMap() {
    return new TreeMap<>(idfMap);
  }

  // returns the idf of a term
  private double idf(String term) {
    return idfMap.get(term);
  }
  // calculates the term frequencies for a document

  // zlicza ilość wystąpień termu (z keywords) w dokumencie
  private int calculateTermFrequencyInDocument(Document document, Document term) {
    long count =
        Arrays.stream(document.getValue().split(" "))
            .filter(s -> term.getValue().equals(s))
            .count();
    return (int) count;
  }

  Map<String, Double> calculateTF(Document doc) {
    Map<String, Integer> termFrequency = new HashMap<>();
    for (Document term : this.termCollection) {
      int i = calculateTermFrequencyInDocument(doc, term);
      termFrequency.put(term.getValue(), i);
    }
    int maxInDoc = termFrequency.values().stream().mapToInt(v -> v).max().orElse(0);
    Map<String, Double> tfResult = new TreeMap<>();
    for (String term : termFrequency.keySet()) {
      if (maxInDoc == 0) {
        tfResult.put(term, 0.);
      } else {
        tfResult.put(term, (double) termFrequency.get(term) / maxInDoc);
      }
    }
    return tfResult;
  }

  public List<Document> getDocumentCollection() {
    return new ArrayList<>(this.documentCollection);
  }

  // ranks a query to the documents of the database
  public Vector<DocScore> rank(Document queryDocument) {
    Map<String, Double> queryTermsFrequencyMap = calculateTF(queryDocument);
    Map<String, Double> queryVecTfIdf = new TreeMap<>();

    for (Map.Entry<String, Double> entry : queryTermsFrequencyMap.entrySet()) {
      String term = entry.getKey();
      double tf = entry.getValue();
      double idf = calculateIdf(entry.getValue().intValue());
      double tfidf = calculateTfIdf(tf, idf);
      queryVecTfIdf.put(term, tfidf);
    }

    double queryVectorLenght = calculateVectorLength(queryVecTfIdf.values());
    Vector<Map<String, Double>> tfIdfMap = this.tfIdfMap;
    Vector<DocScore> result = new Vector<>();
    for (int i = 0; i < tfIdfMap.size(); i++) {
      Map<String, Double> map = tfIdfMap.get(i);
      Double dotProduct = calculateDotProduct(map.values(), queryVecTfIdf.values());
      double currentDocumentLength = calculateVectorLength(map.values());

      double magnitude = queryVectorLenght * currentDocumentLength;

      DocScore docScore = new DocScore(magnitude == 0. ? 0 : dotProduct / magnitude, i);
      result.add(docScore);
    }
    Collections.sort(result);
    return result;
  }

  /**
   * Get relevance feedback using Rocchio algorithm
   *
   * @param queryDocument
   * @param relevantDocuments
   * @param nonRelevantDocuments - when null, then all documents different than relevantDocuments
   *     are considered nonrelevant
   * @return
   */
  public Vector<DocScore> getRelevanceFeedback(
      Document queryDocument,
      Vector<Document> relevantDocuments,
      Vector<Document> nonRelevantDocuments) {
    Map<String, Double> originalQuery = calculateTF(queryDocument);
    // original query vector -> originalQuery.values()

    Map<String, Double> relevantTermFrequency = new TreeMap<>();
    for (Document doc : relevantDocuments) {
      for (Document term : this.termCollection) {
        int i = calculateTermFrequencyInDocument(doc, term);
        relevantTermFrequency.put(
            term.getValue(), i + relevantTermFrequency.getOrDefault(term.getValue(), 0.));
      }
    }

    if (isNull(nonRelevantDocuments)) {
      nonRelevantDocuments =
          documentCollection
              .stream()
              .filter(d -> !relevantDocuments.contains(d))
              .collect(Collectors.toCollection(Vector::new));
    }

    Map<String, Double> nonRelevantTermFrequency = new TreeMap<>();
    for (Document doc : nonRelevantDocuments) {
      for (Document term : this.termCollection) {
        int i = calculateTermFrequencyInDocument(doc, term);
        nonRelevantTermFrequency.put(
            term.getValue(), i + nonRelevantTermFrequency.getOrDefault(term.getValue(), 0.));
      }
    }

    Vector<Double> doubles = calculateRocchio(
        1.,
        0.75,
        0.15,
        new ArrayList<>(originalQuery.values()),
        new ArrayList<>(relevantTermFrequency.values()),
        new ArrayList<>(nonRelevantTermFrequency.values()),
        relevantDocuments.size(),
        nonRelevantDocuments.size());

    Map<String, Double> rocchioQuery = new TreeMap<>();

    for(int i =0 ;i < idfMap.keySet().size(); i++){
      rocchioQuery.put(new ArrayList<>(idfMap.keySet()).get(i), doubles.get(i));
    }

    return rank(rocchioQuery);
  }

  private Vector<DocScore> rank(Map<String, Double> queryTermsFrequencyMap) {
    Map<String, Double> queryVecTfIdf = new TreeMap<>();

    for (Map.Entry<String, Double> entry : queryTermsFrequencyMap.entrySet()) {
      String term = entry.getKey();
      double tf = entry.getValue();
      double idf = calculateIdf(entry.getValue());
      double tfidf = calculateTfIdf(tf, idf);
      queryVecTfIdf.put(term, tfidf);
    }

    double queryVectorLenght = calculateVectorLength(queryVecTfIdf.values());
    Vector<Map<String, Double>> tfIdfMap = this.tfIdfMap;
    Vector<DocScore> result = new Vector<>();
    for (int i = 0; i < tfIdfMap.size(); i++) {
      Map<String, Double> map = tfIdfMap.get(i);
      Double dotProduct = calculateDotProduct(map.values(), queryVecTfIdf.values());
      double currentDocumentLength = calculateVectorLength(map.values());

      double magnitude = queryVectorLenght * currentDocumentLength;

      DocScore docScore = new DocScore(magnitude == 0. ? 0 : dotProduct / magnitude, i);
      result.add(docScore);
    }
    Collections.sort(result);
    return result;
  }

  private Vector<Double> calculateRocchio(
      double a,
      double b,
      double c,
      List<Double> originalQueryVector,
      List<Double> relevantDocVector,
      List<Double> nonRelevantDocVector,
      int relevantDocsCount,
      int nonRelevantDocsCount) {
    List<Double> originalPart = multiplyVector(originalQueryVector, a, null);
    List<Double> relevantPart = multiplyVector(relevantDocVector, b, relevantDocsCount);
    List<Double> nonRelevantPart = multiplyVector(nonRelevantDocVector, c, nonRelevantDocsCount);
    Vector<Double> calulatedQuery = new Vector<>(originalPart.size());

    for (int i = 0; i < originalPart.size(); i++) {
      calulatedQuery.add(originalPart.get(i) + relevantPart.get(i) - nonRelevantPart.get(i));
    }

    return calulatedQuery.stream().map(e -> e < 0 ? 0 : e).collect(Collectors.toCollection(Vector::new));
  }

  private List<Double> multiplyVector(List<Double> vector, double weight, Integer count) {
    if (count == null) {
      count = 1;
    }
    int val = count;
    return vector
        .stream()
        .map(e -> e * weight * (1. / val))
        .collect(Collectors.toCollection(Vector::new));
  }

  private double calculateVectorLength(Collection<Double> collection) {
    double sum = collection.stream().map(e -> Math.pow(e, 2)).mapToDouble(e -> e).sum();
    return Math.sqrt(sum);
  }

  private double calculateDotProduct(Collection<Double> first, Collection<Double> second) {
    if (first.size() != second.size()) {
      throw new RuntimeException("Cannot calculate dot product - different sizes");
    }
    Vector<Double> firstVector = new Vector<>(first);
    Vector<Double> secondVector = new Vector<>(second);
    double result = 0;
    for (int i = 0; i < first.size(); i++) {
      result += firstVector.get(i) * secondVector.get(i);
    }
    return result;
  }
}
